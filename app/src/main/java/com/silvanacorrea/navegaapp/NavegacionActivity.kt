package com.silvanacorrea.navegaapp

import android.graphics.Color
import android.graphics.Typeface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.silvanacorrea.navegaapp.R
import kotlinx.android.synthetic.main.activity_navegacion.*

class NavegacionActivity : AppCompatActivity(), OnNavListener, View.OnClickListener{

    private val FRAGMENT_WEB = 0
    private val FRAGMENT_SHOPPING = 1
    private val FRAGMENT_VIDEO = 2

    private val indicatorViews = mutableListOf<View>()
    private val titleViews = mutableListOf<TextView>()
    private var currentIndicator = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navegacion)

        indicatorViews.add(iviWeb)
        indicatorViews.add(iviShopping)
        indicatorViews.add(iviVideos)

        titleViews.add(tviWeb)
        titleViews.add(tviShopping)
        titleViews.add(tviVideos)

        titleViews.forEach{
            it.setOnClickListener(this)
        }

        selectFirst()

    }

    override fun onClick(view: View?) {

        val bundle = Bundle()

        var fragmentId = when(view?.id){
            R.id.tviWeb -> FRAGMENT_WEB
            R.id.iviShopping -> FRAGMENT_SHOPPING
            R.id.iviVideos -> FRAGMENT_VIDEO

            else -> FRAGMENT_SHOPPING
        }

        updateUI(fragmentId)
        changeFragment(bundle, fragmentId)

    }

    private fun selectFirst(){
        val bundle = Bundle()
        val fragmentId = FRAGMENT_SHOPPING
        updateUI(fragmentId)
        changeFragment(bundle, fragmentId)

    }

    private fun updateUI(fragmentId: Int){
        if (currentIndicator >=0){
            indicatorViews[currentIndicator].setBackgroundColor(Color.TRANSPARENT)
            titleViews[currentIndicator].inputType = Typeface.NORMAL
        }
        indicatorViews[fragmentId].setBackgroundColor(Color.parseColor("#ffeb3b"))
        titleViews[fragmentId].inputType = Typeface.BOLD
        currentIndicator = fragmentId
    }

    private fun changeFragment(bundle: Bundle, fragmentId: Int){

        val fragment = factoryFragment(bundle,fragmentId)

        fragment?.let{
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flayContainer, it)
                addToBackStack(null)
                commit()
            }
        }
    }

    private fun factoryFragment(bundle: Bundle,fragmentId: Int): Fragment?{
        when(fragmentId){
            FRAGMENT_WEB -> return WebFragment()
            FRAGMENT_SHOPPING -> return ShoppingFragment()
            FRAGMENT_VIDEO -> return VideoFragment()
        }

        return null
    }
}