package com.silvanacorrea.navegaapp

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import java.lang.RuntimeException


class ShoppingFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private var listener: OnNavListener? = null


   override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_shopping, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        if(context is OnNavListener){
            listener = context
        } else{
            throw RuntimeException("$context necesita implementar OnNavListener")
        }
    }

    override fun onDetach() {
        super.onDetach()
        listener = null
    }
}